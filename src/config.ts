export default {
  jwt: {
    secretOrKey: '__JWT_SECRET_KEY__',
    expiresIn: 86400,
  },
  // You can also use any other email sending services
  mail: {
    service: {
      host: 'smtp-relay.sendinblue.com',
      port: 587,
      secure: false,
      user: 'teamit.igg@gmail.com',
      pass: '3pCA4jzaIFGM9JbN',
    },
    senderCredentials: {
      name: 'teamit.igg@gmail.com',
      email: 'teamit.igg@gmail.com',
    },
  },
  // these are used in the mail templates
  project: {
    name: 'Rest Server',
    address: 'PT. IGG',
    logoUrl: 'http://rest-server.igg.co.id',
    slogan: 'Made with ❤️ in indonesia',
    color: '#123456',
    socials: [
      ['Gitlab', 'https://gitlab.com/fuadhs/rest-server.git'],
      ['__Social_Media_1__', '__Social_Media_1_URL__'],
      ['__Social_Media_2__', '__Social_Media_2_URL__'],
    ],
    url: 'http://localhost:3000',
    mailVerificationUrl: 'http://localhost:3000/auth/verify',
    mailChangeUrl: 'http://localhost:3000/auth/change-email',
    resetPasswordUrl: 'http://localhost:3000/reset-password',
    termsOfServiceUrl: 'http://localhost:3000/legal/terms',
  },
};
